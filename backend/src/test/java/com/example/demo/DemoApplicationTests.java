package com.example.demo;

import static org.junit.Assert.*;

import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class DemoApplicationTests {
	private MockMvc mockMvc;

	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.standaloneSetup(new DemoApplication()).build();
	}

	@Test
	public void testGetTasks() throws Exception {
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/")
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		int status = result.getResponse().getStatus();
		assertEquals(200, status);
		String content = result.getResponse().getContentAsString();
		List<Task> taskList = new ObjectMapper().readValue(content, new TypeReference<List<Task>>() {
		});
		assertNotNull(taskList);
	}

	@Test
	public void testTaskDate() {
		String description = "Test Task";
		String date = "2022-12-31";
		Task task = new Task();
		task.setTaskdescription(description);
		task.setDate(date);
		Assertions.assertEquals(description, task.getTaskdescription());

	}
}